/*
 * PongView.cpp
 *
 *  Created on: Jun 29, 2018
 *      Author: christoph
 */

#include "PongView.h"

CPongView PongView(PongGame);

void CPongView::initialize(CRGB *const GAME_FIELD, const int GAME_FIELD_LENGTH,
                           CRGB *const NUMBER_FIELD_A,
                           CRGB *const NUMBER_FIELD_B,
                           const int NUMBER_FIELD_LENGTH, const int LED_PIN_A,
                           const int LED_PIN_B, const int BUZZER_PIN) {
  gameField = GAME_FIELD;
  gameFieldLength = GAME_FIELD_LENGTH;
  numberFieldA = NUMBER_FIELD_A;
  numberFieldB = NUMBER_FIELD_B;
  numberFieldLength = NUMBER_FIELD_LENGTH;
  ledPinA = LED_PIN_A;
  ledPinB = LED_PIN_B;
  buzzerPin = BUZZER_PIN;

  digitalWrite(ledPinA, HIGH);
  digitalWrite(ledPinB, HIGH);
}

bool CPongView::showDigit(int fieldNum, int digit, CRGB color, bool invert) {
  // Map of which LEDs need to be active for what digit
  bool digitMap[10][15] = {{1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
                           {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1},
                           {1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1},
                           {1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1},
                           {1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1},
                           {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1}};

  CRGB *field = nullptr;
  if (fieldNum == CPongGame::PLAYER_A)
    field = numberFieldA;
  else if (fieldNum == CPongGame::PLAYER_B)
    field = numberFieldB;
  else
    return false;
  // Display digits
  for (int i = 0; i < numberFieldLength; i++)
    if (digitMap[digit % 10][invert ? numberFieldLength - 1 - i : i])
      field[i] = color;
    else
      field[i] = CRGB::Black;
  return true;
}

bool CPongView::winAnimation(int winner, int itterations, int delayTime) {
  if (itterations < 1 ||
      (winner != CPongGame::PLAYER_A && winner != CPongGame::PLAYER_B))
    return false;

  int animationPath[] = {0, 1, 2, 3, 4, 5, 14, 13, 12, 11, 10, 9};
  int direction, startLed;
  CRGB *winnerField;

  resetLedStrips();
  if (winner == CPongGame::PLAYER_A) {
    digitalWrite(ledPinA, HIGH);
    digitalWrite(ledPinB, LOW);
    direction = -1;
    startLed = gameFieldLength - 1;
    winnerField = numberFieldA;
  } else {
    digitalWrite(ledPinA, LOW);
    digitalWrite(ledPinB, HIGH);
    direction = 1;
    startLed = 0;
    winnerField = numberFieldB;
  }

  for (int i = 0; i < itterations; i++) {
    for (int j = 0; j < gameFieldLength; j++) {
      digitalWrite(buzzerPin, (i * gameFieldLength + j) % 2);
      int colorAngle = (i * gameFieldLength + j) * 255 / gameFieldLength;
      gameField[startLed + (j * direction)].setRGB(
          sin8(colorAngle), sin8(colorAngle + 1. / 3 * 255),
          sin8(colorAngle + 2. / 3 * 255));
      winnerField[animationPath[(i * gameFieldLength + j) %
                                (sizeof(animationPath) / sizeof(int))]]
          .setRGB(sin8(colorAngle + 1. / 3 * 255),
                  sin8(colorAngle + 2. / 3 * 255), sin8(colorAngle));

      FastLED.show();
      delay(delayTime);

      gameField[startLed + (j * direction)] = CRGB::Black;
      winnerField[animationPath[(i * gameFieldLength + j) %
                                (sizeof(animationPath) / sizeof(int))]] =
          CRGB::Black;
    }
  }
  digitalWrite(buzzerPin, LOW);
  return true;
}

void CPongView::resetLedStrips() {
  resetGameField();
  resetNumberFields();
}

void CPongView::resetGameField() {
  for (int i = 0; i < gameFieldLength; i++)
    gameField[i] = CRGB::Black;
}

void CPongView::resetNumberFields() {
  for (int i = 0; i < numberFieldLength; i++) {
    numberFieldA[i] = CRGB::Black;
    numberFieldB[i] = CRGB::Black;
  }
}

bool CPongView::ballPosition() {
  CRGB color;
  int hitAreaSize = game->getLength() * 0.5 * game->getDifficulty();
  int position = game->getPosition();
  float velocity = game->getVelocity();
  // Boostable
  if (game->isBoostable())
    color = colorScheme.getBoostColor();
  // Warning area A
  else if (velocity < 0 && position < hitAreaSize / 2)
    color = colorScheme.getWarningColor();
  // Waring area B
  else if (velocity > 0 && position >= gameFieldLength - hitAreaSize / 2)
    color = colorScheme.getWarningColor();
  // Hit area A
  else if (velocity < 0 && position < hitAreaSize)
    color = colorScheme.getHitAreaColor();
  // Hit area B
  else if (velocity > 0 && position >= gameFieldLength - hitAreaSize)
    color = colorScheme.getHitAreaColor();
  // Boost
  else if (game->isBoost())
    color = colorScheme.getBoostColor();
  // Normal ball
  else
    color = colorScheme.getBallColor();

  return ballPosition(position, color);
}

bool CPongView::ballPosition(int position, CRGB color) {
  if (position < 0 || position >= gameFieldLength)
    return false;

  resetGameField();
  gameField[position] = color;
  FastLED.show();

  return true;
}

void CPongView::refresh() {
  if (game->isWon()) {
    int winner = (game->getScore(CPongGame::PLAYER_A) >
                  game->getScore(CPongGame::PLAYER_B))
                     ? CPongGame::PLAYER_A
                     : CPongGame::PLAYER_B;
    winAnimation(winner);
    game->reset();
    return;
  }

  ballPosition();
  showDigit(CPongGame::PLAYER_A, game->getScore(CPongGame::PLAYER_A),
            colorScheme.getPlayerColor(CPongGame::PLAYER_A));
  if (game->getGameMode() == CPongGame::TWO_PLAYER) {
    digitalWrite(ledPinA, HIGH);
    digitalWrite(ledPinB, HIGH);
    showDigit(CPongGame::PLAYER_B, game->getScore(CPongGame::PLAYER_B),
              colorScheme.getPlayerColor(CPongGame::PLAYER_B));
  } else if (game->getGameMode() == CPongGame::ONE_PLAYER) {
    digitalWrite(ledPinA, HIGH);
    digitalWrite(ledPinB, LOW);
    showDigit(CPongGame::PLAYER_B, game->getScore(CPongGame::PLAYER_B),
              colorScheme.getPlayerColor(CPongGame::PLAYER_B), true);
  }
  if (game->getScore(CPongGame::PLAYER_A) != lastScore[CPongGame::PLAYER_A] ||
      game->getScore(CPongGame::PLAYER_B) != lastScore[CPongGame::PLAYER_B])
    digitalWrite(buzzerPin, HIGH);
  else
    digitalWrite(buzzerPin, LOW);
  lastScore[CPongGame::PLAYER_A] = game->getScore(CPongGame::PLAYER_A);
  lastScore[CPongGame::PLAYER_B] = game->getScore(CPongGame::PLAYER_B);
}

void CPongView::showHitArea(bool standalone) {
  if (standalone)
    resetLedStrips();
  int hitAreaSize = gameFieldLength * 0.5 * game->getDifficulty();
  for (int i = 0; i < gameFieldLength; i++)
    if (i < hitAreaSize || i >= gameFieldLength - hitAreaSize) {
      if (i < hitAreaSize / 2 || i >= gameFieldLength - hitAreaSize / 2)
        gameField[i] = colorScheme.getWarningColor();
      else
        gameField[i] = colorScheme.getHitAreaColor();
    }
  if (standalone)
    FastLED.show();
}

void CPongView::showGameMode(bool standalone) {
  if (standalone)
    resetLedStrips();
  switch (game->getGameMode()) {
  case CPongGame::ONE_PLAYER:
    digitalWrite(ledPinA, HIGH);
    digitalWrite(ledPinB, LOW);
    for (int i = 0; i < numberFieldLength; i++)
      numberFieldA[i] = colorScheme.getPlayerColor(CPongGame::PLAYER_A);
    break;
  case CPongGame::TWO_PLAYER:
    digitalWrite(ledPinA, HIGH);
    digitalWrite(ledPinB, HIGH);
    for (int i = 0; i < numberFieldLength; i++) {
      numberFieldA[i] = colorScheme.getPlayerColor(CPongGame::PLAYER_A);
      numberFieldB[i] = colorScheme.getPlayerColor(CPongGame::PLAYER_B);
    }
    break;
  }
  if (standalone)
    FastLED.show();
}

void CPongView::showOpponentLevel(bool standalone) {
  if (standalone)
    resetLedStrips();
  for (int i = 0; i < gameFieldLength; i++)
    if (i > (1 - game->getOpponentLevel()) * gameFieldLength) {
      if (i < 0.2 * gameFieldLength)
        gameField[i] = colorScheme.getHardColor();
      else if (i < 0.4 * gameFieldLength)
        gameField[i] = colorScheme.getHighMiddleColor();
      else if (i < 0.6 * gameFieldLength)
        gameField[i] = colorScheme.getMiddleColor();
      else if (i < 0.8 * gameFieldLength)
        gameField[i] = colorScheme.getLowMiddleColor();
      else
        gameField[i] = colorScheme.getEasyColor();
    }
  if (standalone)
    FastLED.show();
}

void CPongView::showStartVelocity(float upperBound, bool standalone) {
  if (standalone)
    resetLedStrips();
  for (int i = 0; i < gameFieldLength; i++)
    if (i < game->getStartVelocity() / upperBound * gameFieldLength) {
      if (i > 0.8 * gameFieldLength)
        gameField[i] = colorScheme.getHardColor();
      else if (i > 0.6 * gameFieldLength)
        gameField[i] = colorScheme.getHighMiddleColor();
      else if (i > 0.4 * gameFieldLength)
        gameField[i] = colorScheme.getMiddleColor();
      else if (i > 0.2 * gameFieldLength)
        gameField[i] = colorScheme.getLowMiddleColor();
      else
        gameField[i] = colorScheme.getEasyColor();
    }
  if (standalone)
    FastLED.show();
}

void CPongView::showWinPoints(bool standalone) {
  if (standalone)
    resetLedStrips();
  int winPoints = game->getWinPoints();
  showDigit(CPongGame::PLAYER_A, winPoints, colorScheme.getNeutralColor());
  showDigit(CPongGame::PLAYER_B, winPoints, colorScheme.getNeutralColor());
  if (standalone)
    FastLED.show();
}

void CPongView::showReady(bool standalone) {
  if (standalone)
    resetLedStrips();
  showHitArea(false);
  showDigit(CPongGame::PLAYER_A, 0,
            colorScheme.getPlayerColor(CPongGame::PLAYER_A));
  showDigit(CPongGame::PLAYER_B, 0,
            colorScheme.getPlayerColor(CPongGame::PLAYER_B));
  if (standalone)
    FastLED.show();
}

PongColorScheme::PongColorScheme(CRGB playerAColor, CRGB playerBColor,
                                 CRGB ballColor, CRGB boostColor,
                                 CRGB hitAreaColor, CRGB warningColor)
    : ballColor(ballColor), boostColor(boostColor), hitAreaColor(hitAreaColor),
      warningColor(warningColor) {
  playerColors[CPongGame::PLAYER_A] = playerAColor;
  playerColors[CPongGame::PLAYER_B] = playerBColor;
}
