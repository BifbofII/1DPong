# 1D-Pong

![Game Field](pictures/1DPong_10.jpg)

This is the code for a one dimensional pong game based on [this](https://www.instructables.com/id/Make-Your-Own-1D-Pong-Game/) project by GreatScott. Credit for the game ides goes to GreatScott, I altert the design of the physical build and wrote my own code from scratch.

This altered code adds different adjustable game parameters and a signle player mode.

A manual can be found as a PDF document, but for now only in german language.
