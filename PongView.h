/*
 * PongView.h
 *
 *  Created on: Jun 29, 2018
 *      Author: christoph
 */

#ifndef CPONGVIEW_H_
#define CPONGVIEW_H_

#include "Arduino.h"
#include "FastLED.h"

#include "PongGame.h"

class PongColorScheme {
public:
  PongColorScheme() {}
  PongColorScheme(CRGB playerAColor, CRGB playerBColor, CRGB ballColor,
                  CRGB boostColor, CRGB hitAreaColor, CRGB warningColor);
  // Getter
  CRGB getPlayerColor(int playerNumber) { return playerColors[playerNumber]; }
  CRGB getBallColor() { return ballColor; }
  CRGB getBoostColor() { return boostColor; }
  CRGB getHitAreaColor() { return hitAreaColor; }
  CRGB getWarningColor() { return warningColor; }
  CRGB getEasyColor() { return easyColor; }
  CRGB getLowMiddleColor() { return lowMiddleColor; }
  CRGB getMiddleColor() { return middleColor; }
  CRGB getHighMiddleColor() { return highMiddleColor; }
  CRGB getHardColor() { return hardColor; }
  CRGB getNeutralColor() { return neutralColor; }
  // Setter
  void setPlayerColor(int playerNumber, CRGB color) {
    playerColors[playerNumber] = color;
  }
  void setBallColor(CRGB color) { ballColor = color; }
  void setBoostColor(CRGB color) { boostColor = color; }
  void setHitAreaColor(CRGB color) { hitAreaColor = color; }
  void setWarningColor(CRGB color) { warningColor = color; }
  void setEaysColor(CRGB color) { easyColor = color; }
  void setLowMiddleColor(CRGB color) { lowMiddleColor = color; }
  void setMiddleColor(CRGB color) { middleColor = color; }
  void setHighMiddleColor(CRGB color) { highMiddleColor = color; }
  void setHardColor(CRGB color) { hardColor = color; }
  void setNeutralColor(CRGB color) { neutralColor = color; }

private:
  CRGB playerColors[2] = {CRGB::Red, CRGB::Blue};
  CRGB ballColor = CRGB::Cyan;
  CRGB boostColor = CRGB::DeepPink;
  CRGB hitAreaColor = CRGB::Yellow;
  CRGB warningColor = CRGB::OrangeRed;
  CRGB easyColor = CRGB::Green;
  CRGB lowMiddleColor = CRGB::Yellow;
  CRGB middleColor = CRGB::Orange;
  CRGB highMiddleColor = CRGB::OrangeRed;
  CRGB hardColor = CRGB::Red;
  CRGB neutralColor = CRGB::Green;
};

class CPongView {
public:
  CPongView(CPongGame &game) : game(&game) {}
  bool showDigit(int fieldNum, int digit, CRGB color, bool invert = false);
  bool winAnimation(int winner, int itterations = 4, int delayTime = 20);
  bool ballPosition(int position, CRGB color);
  void showHitArea(bool standalone = true);
  void showGameMode(bool standalone = true);
  void showOpponentLevel(bool standalone = true);
  void showStartVelocity(float upperBound = 20, bool standalone = true);
  void showWinPoints(bool standalone = true);
  void showReady(bool standalone = true);
  void refresh();
  void initialize(CRGB *const GAME_FIELD, const int GAME_FIELD_LENGTH,
                  CRGB *const NUMBER_FIELD_A, CRGB *const NUMBER_FIELD_B,
                  const int NUMBER_FIELD_LENGTH, const int LED_A_PIN,
                  const int LED_B_PIN, const int BUZZER_PIN);
  void setColorScheme(PongColorScheme colorScheme) {
    this->colorScheme = colorScheme;
  }

private:
  CPongGame *game = nullptr;
  CRGB *gameField = nullptr;
  int gameFieldLength = 0;
  CRGB *numberFieldA = nullptr;
  CRGB *numberFieldB = nullptr;
  int numberFieldLength = 0;
  int ledPinA = 0;
  int ledPinB = 0;
  int buzzerPin = 0;
  int lastScore[2] = {0, 0};
  PongColorScheme colorScheme;

  void resetLedStrips();
  void resetGameField();
  void resetNumberFields();
  bool ballPosition();
};

extern CPongView PongView;

#endif /* CPONGVIEW_H_ */
