/*
 * PongGame.h
 *
 *  Created on: Jun 25, 2018
 *      Author: christoph
 */

#ifndef CPONGGAME_H_
#define CPONGGAME_H_

#include "Arduino.h"
#include "TimerOne.h"

class CPongGame {
public:
  CPongGame();
  virtual ~CPongGame() {}
  // Getter
  // Configuration values
  int getLength() { return length; }
  float getDifficulty() { return difficulty; }
  float getOpponentLevel() { return opponentLevel; }
  float getStartVelocity() { return startVelocity; }
  int getWinPoints() { return winPoints; }
  // State values
  bool isBoost() { return boost; }
  bool isBoostable() { return boostable; }
  bool getDirection() { return velocity > 0; }
  float getVelocity() { return velocity; }
  int getPosition() { return position; }
  int getScore(int playerNumber);
  int getGameMode() { return gameMode; }
  bool isWon() { return won; }
  // Setter
  void setLength(int length) { this->length = length; };
  void setStartVelocity(float startVelocity) {
    this->startVelocity = startVelocity;
  };
  void setDifficulty(float difficulty) { this->difficulty = difficulty; }
  void setOpponentLevel(float opponentLevel) {
    this->opponentLevel = opponentLevel;
  }
  bool setGameMode(int gameMode);
  void setWinPoints(int winPoints) { this->winPoints = winPoints; }
  // Functionality
  void reset(); // Reset the game
  bool
  buttonPressed(int playerNumber); // Method to call if a button was pressed
  void start() { running = true; }
  void stop() { running = false; }
  // Constants
  // Game states
  static const int ONE_PLAYER = 1;
  static const int TWO_PLAYER = 2;
  // Player identifiers
  static const int PLAYER_A = 0;
  static const int PLAYER_B = 1;
  // Index out of bounds
  static const int INDEX_OUT_OF_BOUNDS;

private:
  static void timerInterrupt();
  void move();
  void setVelocity(float velocity, bool boost = false);
  bool incScore(int playerNumber);
  bool playerAction(int playerNumber); // Method that contains the action that
                                       // is taken if a player pushes a button
  void calculateOpponentHit();
  // Configuration values
  int length = 20;           // The length of the game
  float startVelocity = 15;  // The velocity at which a new game starts
  int gameMode = TWO_PLAYER; // Current state of the game
  float difficulty = 0.4; // Difficulty of the game (defines size of hit area)
  float opponentLevel = 0.5; // Level of the simulated opponent
  const float BOOST_FACTOR =
      2; // The factor by which the speed is increased in boost mode
  const float SPEED_INCREASE_FACTOR =
      1.05; // The factor by which the speed is increased each hit
  // Variables for debouncing buttons
  const int DEBOUNCE_TIME = 200; // Minimum time between two button inputs
  unsigned long lastInterrupt[2] = {0, 0}; // Last time the interrupt was called
  // Configuration values for opponent algorithm
  // For visual representation of the algorithm see:
  // https://www.desmos.com/calculator/sjwwaj4uad
  const float MIN_PROB =
      0.2; // Minimum probability that the opponent always has [0-1]
  const float K_E = 0.2; // Angle of the logistic slope [0-2]
  const float K_OL =
      45; // After that many hits, the perfect opponent has the middle
          // probability value / Main adjustment value [20-100]
  const float K_D = 0.5; // Middle probability value [0-1]
  const float K_H = 8;   // "Standard" hit area size
  // State values
  bool running = true; // Whether the game is running or not
  int position = 0;    // The position of the ball
  float velocity = 0;  // Velocity of the ball in pixels/s
  bool boost = false;  // Whether or not he ball is in boost mode
  bool boostable =
      false; // Whether or not the ball is boostable on the current field
  int nextBoostable = 0;   // The next position at which the ball can be boosted
  int nextOpponentHit = 0; // The field at which the opponent will hit next time
  int score[2] = {0, 0};   // The points of the two players
  int winPoints = 10;      // The number of points needed to win
  bool won = false;        // Wether the game is won or not
};

extern CPongGame PongGame;

#endif /* CPONGGAME_H_ */
