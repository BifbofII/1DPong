/*
 * PongGame.cpp
 *
 *  Created on: Jun 25, 2018
 *      Author: christoph
 */

#include "PongGame.h"

CPongGame PongGame;

CPongGame::CPongGame() {}

void CPongGame::reset() { // Reset the game
  // Initialize timer
  Timer1.stop();
  Timer1.attachInterrupt(timerInterrupt);

  running = true;
  position = 0;
  boost = false;
  boostable = false;
  won = false;
  setVelocity(0);
  score[0] = 0;
  score[1] = 0;

  calculateOpponentHit();
}

void CPongGame::timerInterrupt() { PongGame.move(); }

void CPongGame::move() {
  if (velocity == 0 || !running)
    return;

  if (position == nextOpponentHit && velocity > 0 && gameMode == ONE_PLAYER) {
    playerAction(PLAYER_B);
  }

  // Perform move
  position += (velocity > 0) ? 1 : -1;
  if (position == nextBoostable) {
    boostable = true;
  } else
    boostable = false;

  if (position < 0) {
    incScore(PLAYER_B);
    position = 0;
    setVelocity(0);
  } else if (position >= length) {
    incScore(PLAYER_A);
    position = length - 1;
    setVelocity(0);
    if (gameMode == ONE_PLAYER) {
      playerAction(PLAYER_B);
    }
  }
}

void CPongGame::setVelocity(float velocity, bool boost) {
  this->velocity = velocity;
  this->boost = boost;

  // Calculate next boostable field
  int hitAreaSize = length * 0.5 * difficulty;
  if (velocity < 0)
    nextBoostable = random() % hitAreaSize;
  else if (velocity > 0)
    nextBoostable = length - 1 - (random() % hitAreaSize);

  if (velocity == 0)
    Timer1.stop();
  else {
    Timer1.initialize(1000000 / abs(velocity * (boost ? BOOST_FACTOR : 1)));
  }
}

bool CPongGame::buttonPressed(
    int playerNumber) { // Method to call if a button was pressed
  if ((playerNumber == PLAYER_A ||
       (playerNumber == PLAYER_B && gameMode == TWO_PLAYER)) &&
      running) {
    // Debounce
    if (millis() < lastInterrupt[playerNumber] + DEBOUNCE_TIME)
      return false;
    lastInterrupt[playerNumber] = millis();

    // React to button press
    return playerAction(playerNumber);
  }
  return false;
}

bool CPongGame::playerAction(
    int playerNumber) { // Method that contains the action that is
                        // taken if a player pushes a button
  if (playerNumber != PLAYER_A && playerNumber != PLAYER_B)
    return false;

  int hitAreaSize = length * 0.5 * difficulty;
  switch (playerNumber) {
  case PLAYER_A:
    if (velocity < 0) {
      if (position < hitAreaSize) {
        setVelocity(abs(velocity) * SPEED_INCREASE_FACTOR, boostable);
        Timer1.restart();
      } else {
        incScore(PLAYER_B);
        setVelocity(0);
        position = 0;
      }
    } else if (position == 0 && velocity == 0)
      setVelocity(startVelocity);
    if (gameMode == ONE_PLAYER)
      calculateOpponentHit();
    break;
  case PLAYER_B:
    if (velocity > 0) {
      if (position >= length - hitAreaSize) {
        setVelocity(-(abs(velocity) * SPEED_INCREASE_FACTOR), boostable);
        Timer1.restart();
      } else {
        incScore(PLAYER_A);
        setVelocity(0);
        position = length - 1;
      }
    } else if (position == length - 1 && velocity == 0)
      setVelocity(-startVelocity);
    break;
  }
  return true;
}

int CPongGame::getScore(int playerNumber) {
  if (playerNumber != PLAYER_A && playerNumber != PLAYER_B)
    return INDEX_OUT_OF_BOUNDS;
  return score[playerNumber];
}

bool CPongGame::incScore(int playerNumber) {
  if (playerNumber != PLAYER_A && playerNumber != PLAYER_B)
    return false;
  score[playerNumber]++;
  if (score[playerNumber] == winPoints) {
    running = false;
    won = true;
  }
  return true;
}

bool CPongGame::setGameMode(int gameMode) {
  if (gameMode != TWO_PLAYER && gameMode != ONE_PLAYER)
    return false;
  this->gameMode = gameMode;
  return true;
}

void CPongGame::calculateOpponentHit() {
  int hitAreaSize = length * 0.5 * difficulty;
  float prob =
      (1 - MIN_PROB) * 1. /
          (1 + exp(K_E * (1 - MIN_PROB) *
                   (abs(velocity * (boost ? BOOST_FACTOR : 1)) -
                    hitAreaSize / K_H * startVelocity *
                        pow(SPEED_INCREASE_FACTOR, K_OL * opponentLevel))) *
                   (1. / K_D - 1)) +
      MIN_PROB;
  Serial.println(prob);
  if ((random() % 1001) / 1000. < prob)
    nextOpponentHit = length - 1 - random() % hitAreaSize;
  else
    nextOpponentHit = length;
}
