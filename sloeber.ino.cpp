#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-08-21 20:25:07

#include "Arduino.h"
#include "Arduino.h"
#include "EEPROM.h"
#include "Entropy.h"
#include "FastLED.h"
#include "PongGame.h"
#include "PongView.h"

void interruptRed() ;
void interruptBlue() ;
void menu() ;
void setup() ;
void loop() ;

#include "1DPong.ino"


#endif
