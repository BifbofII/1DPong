#include "Arduino.h"
#include "EEPROM.h"
#include "Entropy.h"
#include "FastLED.h"

#include "PongGame.h"
#include "PongView.h"

// Game field config
const int GAME_FIELD_LENGTH = 39;
const int GAME_FIELD_PIN = 4;
CRGB gameField[GAME_FIELD_LENGTH];

// Number field config
const int NUMBER_FIELD_LENGTH = 15;
const int NUMBER_FIELD_RED_PIN = 5;
const int NUMBER_FIELD_BLUE_PIN = 6;
CRGB numberFieldRed[NUMBER_FIELD_LENGTH];
CRGB numberFieldBlue[NUMBER_FIELD_LENGTH];

// Button config
const int BUTTON_RED_PIN = 2;
const int BUTTON_BLUE_PIN = 3;
const int LED_RED_PIN = A0;
const int LED_BLUE_PIN = A1;

// Controls config
const int CONFIG_SWITCH_PIN = 8;
const int CONFIG_BUTTON_PIN = 7;
const int CONFIG_DIAL_PIN = A3;

// Buzzer
const int BUZZER_PIN = A2;

// EEPROM addresses
const int GAME_MODE_ADDRESS = 0;
const int WIN_POINTS_ADDRESS = 4;
const int DIFFICULTY_ADDRESS = 8;
const int START_VELOCITY_ADDRESS = 16;
const int OPPONENT_LEVEL_ADDRESS = 20;

// Interrupt function for red button
void interruptRed() { PongGame.buttonPressed(CPongGame::PLAYER_A); }

// Interrupt function for blue button
void interruptBlue() { PongGame.buttonPressed(CPongGame::PLAYER_B); }

// Settings menu
void menu() {
  const int GAME_MENU = 0;
  const int GAME_MODE_MENU = 1;
  const int WIN_POINTS_MENU = 2;
  const int DIFFICULTY_MENU = 3;
  const int START_VELOCITY_MENU = 4;
  const int OPPONENT_LEVEL_MENU = 5;

  int subMenu = 0;
  bool finished = false;

  while (!finished) {
    switch (subMenu) {
    case GAME_MENU:
      PongView.showReady();
      if (!digitalRead(CONFIG_SWITCH_PIN)) {
        // Leave menu
        // Write new settings to EEPROM
        EEPROM.put(GAME_MODE_ADDRESS, PongGame.getGameMode());
        EEPROM.put(WIN_POINTS_ADDRESS, PongGame.getWinPoints());
        EEPROM.put(DIFFICULTY_ADDRESS, PongGame.getDifficulty());
        EEPROM.put(START_VELOCITY_ADDRESS, PongGame.getStartVelocity());
        EEPROM.put(OPPONENT_LEVEL_ADDRESS, PongGame.getOpponentLevel());
        // Reset Game
        PongGame.reset();
        finished = true;
      }
      break;
    case GAME_MODE_MENU:
      PongView.showGameMode();
      if (!digitalRead(CONFIG_SWITCH_PIN))
        PongGame.setGameMode(digitalRead(CONFIG_DIAL_PIN)
                                 ? CPongGame::ONE_PLAYER
                                 : CPongGame::TWO_PLAYER);
      break;
    case WIN_POINTS_MENU:
      PongView.showWinPoints();
      if (!digitalRead(CONFIG_SWITCH_PIN))
        PongGame.setWinPoints(int(analogRead(CONFIG_DIAL_PIN) / 102.4) + 1);
      break;
    case DIFFICULTY_MENU:
      PongView.showHitArea();
      if (!digitalRead(CONFIG_SWITCH_PIN))
        PongGame.setDifficulty(analogRead(CONFIG_DIAL_PIN) / 1023.);
      break;
    case START_VELOCITY_MENU:
      PongView.showStartVelocity();
      if (!digitalRead(CONFIG_SWITCH_PIN))
        PongGame.setStartVelocity(analogRead(CONFIG_DIAL_PIN) / 1023. * 20);
      break;
    case OPPONENT_LEVEL_MENU:
      PongView.showOpponentLevel();
      if (!digitalRead(CONFIG_SWITCH_PIN))
        PongGame.setOpponentLevel(analogRead(CONFIG_DIAL_PIN) / 1023.);
      break;
    }

    if (!digitalRead(CONFIG_BUTTON_PIN)) {
      // Next submenu
      subMenu++;
      while (!digitalRead(CONFIG_BUTTON_PIN)) {
        delay(10);
      }
      if (subMenu > OPPONENT_LEVEL_MENU)
        // Sub menu wrap around
        subMenu = 0;
      else if (subMenu == OPPONENT_LEVEL_MENU && PongGame.getGameMode() == CPongGame::TWO_PLAYER)
        // Sub menu wrap around (two player mode)
        subMenu = 0;
    }
    delay(100);
  }
}

void setup() {
  // Serial config
  Serial.begin(9600);

  // Seed for random generator
  Entropy.initialize();
  randomSeed(Entropy.random());

  // Game field config
  FastLED.addLeds<WS2812B, GAME_FIELD_PIN, GRB>(gameField, GAME_FIELD_LENGTH);

  // Number field config
  FastLED.addLeds<WS2812B, NUMBER_FIELD_RED_PIN, GRB>(numberFieldRed,
                                                      NUMBER_FIELD_LENGTH);
  FastLED.addLeds<WS2812B, NUMBER_FIELD_BLUE_PIN, GRB>(numberFieldBlue,
                                                       NUMBER_FIELD_LENGTH);

  // Button config
  pinMode(BUTTON_RED_PIN, INPUT_PULLUP);
  pinMode(BUTTON_BLUE_PIN, INPUT_PULLUP);
  attachInterrupt(0, interruptRed, FALLING);
  attachInterrupt(1, interruptBlue, FALLING);
  pinMode(LED_RED_PIN, OUTPUT);
  pinMode(LED_BLUE_PIN, OUTPUT);

  // Controls config
  pinMode(CONFIG_SWITCH_PIN, INPUT_PULLUP);
  pinMode(CONFIG_BUTTON_PIN, INPUT_PULLUP);
  pinMode(CONFIG_DIAL_PIN, INPUT);

  // Buzzer config
  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(BUZZER_PIN, LOW);

  // Configure game
  PongGame.setLength(GAME_FIELD_LENGTH);
  PongGame.reset();

  // Configure view
  PongView.initialize(gameField, GAME_FIELD_LENGTH, numberFieldRed,
                      numberFieldBlue, NUMBER_FIELD_LENGTH, LED_RED_PIN,
                      LED_BLUE_PIN, BUZZER_PIN);

  // Read last settings from EEPROM
  int gameMode, winPoints;
  float difficulty, startVelocity, opponentLevel;
  // Read EEPROM
  EEPROM.get(GAME_MODE_ADDRESS, gameMode);
  EEPROM.get(WIN_POINTS_ADDRESS, winPoints);
  EEPROM.get(DIFFICULTY_ADDRESS, difficulty);
  EEPROM.get(START_VELOCITY_ADDRESS, startVelocity);
  EEPROM.get(OPPONENT_LEVEL_ADDRESS, opponentLevel);
  // Set values
  PongGame.setGameMode(gameMode);
  PongGame.setWinPoints(winPoints);
  PongGame.setDifficulty(difficulty);
  PongGame.setStartVelocity(startVelocity);
  PongGame.setOpponentLevel(opponentLevel);
}

void loop() {
  if (digitalRead(CONFIG_SWITCH_PIN))
    // Configuration mode
    menu();

  PongView.refresh();

  delay(10);
}
